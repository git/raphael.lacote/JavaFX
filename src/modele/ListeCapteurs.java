package modele;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static javafx.collections.FXCollections.observableArrayList;

public class ListeCapteurs {

    public ListeCapteurs(){}

    private ObservableList<Capteur> lesCapteurs= observableArrayList();

    public ObservableList<Capteur> getLesCapteurs() {
        return lesCapteurs;
    }
}
