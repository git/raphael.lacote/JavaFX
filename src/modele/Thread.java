package modele;

public class Thread extends java.lang.Thread {

    private Capteur c;

    public Thread(Capteur c){
        this.c=c;
    }

    public Capteur getC(){
        return c;
    }

    public void run() {
        try {
            while(true) {
                sleep(c.getTime());
                System.out.print(c.strats.modifieTmp());
                System.out.print("\n");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}