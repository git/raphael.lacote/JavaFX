package modele;

public class TemperatureRandom extends StrategieTemperature {
    public TemperatureRandom(){}
    public double modifieTmp(){
        return Math.random()*20;
    }

    @Override
    public String toString() {
        return "Temperature aléatoire";
    }

}
