package modele;

public class CapteurBasique extends Capteur {

    private Thread threadTime;
    public CapteurBasique(int idUnique,String nom, StrategieTemperature stratsGeneration,int time){
        super(idUnique,nom,stratsGeneration,time);
    }

    public CapteurBasique(int idUnique,String nom){
        super(idUnique, nom);
    }
}
