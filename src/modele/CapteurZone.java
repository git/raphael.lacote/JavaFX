package modele;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CapteurZone extends Capteur {

    public CapteurZone(int idUnique,String nom){
        super(idUnique,nom);
    }
    private Map<Capteur, Double> lesCapteurs= new TreeMap<>();

    public void ajouterCapteur(Capteur NouvCapteur,double poids){
        lesCapteurs.put(NouvCapteur,poids);
    }

    public void retirerCapteur(Capteur CapteurSupp){
        lesCapteurs.remove(CapteurSupp);
    }

    public void calculerMoyennePondere(){

        double moyenne=0.0;

        Set<Map.Entry<Capteur, Double>> elements = lesCapteurs.entrySet();

        Iterator it = elements.iterator();

        while(it.hasNext()) {
            Map.Entry mentry = (Map.Entry)it.next();
            Capteur c = (Capteur) mentry.getKey();

            moyenne= moyenne+(double) mentry.getValue()*c.getTemperature();
        }

        this.setTemperature(moyenne/ lesCapteurs.size());
    }
}
