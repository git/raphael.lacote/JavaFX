package modele;

public abstract class Capteur implements Comparable<Capteur>{

    @Override public int compareTo(Capteur c) {
        return Integer.compare(this.idUnique, c.getIdUnique());
    }

    private int idUnique;
    private String nom;
    private double temperature;
    private int time;
    protected StrategieTemperature strats;

    Capteur(int idUnique,String nom){
        this.idUnique=idUnique;
        this.nom=nom;
        this.temperature=0;
        this.time=0;
    }

    Capteur(int idUnique,String nom,StrategieTemperature stratsGeneration,int time){
        this.idUnique=idUnique;
        this.nom=nom;
        this.time=time;
        changementStrats(stratsGeneration);
    }
    public int getIdUnique(){return this.idUnique;}
    public String getNom(){return this.nom;}
    public double getTemperature(){return this.temperature;}
    public void setTemperature(double nouvTemperature){this.temperature=nouvTemperature;}
    public int getTime(){return this.time;}
    public void setTime(int temps){this.time=temps;}
    public StrategieTemperature getStrats(){return this.strats;}
    public void changementStrats(StrategieTemperature nouvStrats){
        this.strats=nouvStrats;
        calculTemperature();
    }
    public void calculTemperature(){
        this.temperature=this.strats.modifieTmp();
    }


}