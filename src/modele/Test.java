package modele;

public class Test {
    public Test(){};

    public void testFonction(){
        TemperatureRandom r1 = new TemperatureRandom();
        TemperatureCPU c1 = new TemperatureCPU();

        Capteur c = new CapteurBasique(1,"Random",r1,4000);
        Capteur c2 = new CapteurBasique(2,"RandomB",r1,1000);
        Capteur c3 = new CapteurBasique(3,"Fixe",c1,2000);
        CapteurZone z = new CapteurZone(4,"pif");

        Thread t1 = new Thread(c);
        t1.start();

        z.ajouterCapteur(c,4.5);
        z.ajouterCapteur(c3,7.0);
        z.ajouterCapteur(c2,2.3);
        z.retirerCapteur(c);
        z.calculerMoyennePondere();

        System.out.print(z.getTemperature());
        System.out.print("Et paf!!!");
    }
}
