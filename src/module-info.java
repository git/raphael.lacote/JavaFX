module main {
    requires javafx.controls;
    requires javafx.fxml;


    opens main to javafx.fxml;
    exports main;
    /*opens modele to javafx.fxml;
    exports  modele;*/
    exports action;
    opens action to javafx.fxml;

}
