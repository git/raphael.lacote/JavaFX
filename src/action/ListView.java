package action;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import modele.*;
import modele.Thread;

import java.net.URL;
import java.util.ResourceBundle;

public class ListView implements Initializable{
    ObservableList<Capteur> list= FXCollections.observableArrayList();

    @FXML
    private Thread threadElu;

    @FXML
    private Label nameC;

    @FXML
    private Label idC;

    @FXML
    private Label temperatureC;

    @FXML
    private Label strategieC;

    @FXML
    private Label timeC;

    @FXML
    private javafx.scene.control.ListView<String> listeVisible = new javafx.scene.control.ListView<String>();
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadData();
    }

    @FXML
    private void loadData(){
        TemperatureRandom r1 = new TemperatureRandom();
        TemperatureCPU c1 = new TemperatureCPU();

        Capteur c = new CapteurBasique(1,"Capteur A-R",r1,4000);
        Capteur c2 = new CapteurBasique(2,"Capteur B-R",r1,1000);
        Capteur c3 = new CapteurBasique(3,"Capteur C-F",c1,2000);
        CapteurZone z = new CapteurZone(4,"Capteur D-G");

        z.ajouterCapteur(c,4.5);
        z.ajouterCapteur(c3,7.0);

        list.removeAll(list);
        list.add(c);
        list.add(c2);
        list.add(c3);
        list.add(z);

        for (Capteur t:list) {
            this.listeVisible.getItems().add(t.getNom());
        }

    }
    @FXML
    private void onButtonClick(){
        if(threadElu!=null) {
            this.threadElu.stop();
        }
        for(Capteur t:list){
            if(t.getNom()==listeVisible.getSelectionModel().getSelectedItem()){
                this.threadElu=new Thread(t);
                this.idC.setText(String.valueOf(threadElu.getC().getIdUnique()));
                this.nameC.setText(threadElu.getC().getNom());
                this.temperatureC.setText(String.valueOf(threadElu.getC().getTemperature()));
                this.timeC.setText(String.valueOf(threadElu.getC().getTime()));
                this.strategieC.setText(String.valueOf(threadElu.getC().getStrats()));
            }
        }
    }

    @FXML
    public void startThreadButton() {
        if(threadElu!=null){
            threadElu.start();
        }
    }



}
