package action;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class BasicButton {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onButtonClick() {

        welcomeText.setText("Welcome to this application!");
    }
}
