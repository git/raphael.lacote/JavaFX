package main;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Application;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/vues/MainWindow.fxml"));
        Scene pagePrincipale = new Scene(fxmlLoader.load(), 1000, 750);

        stage.setTitle("Captapplication");
        stage.setScene(pagePrincipale);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}